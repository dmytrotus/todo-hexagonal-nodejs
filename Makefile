## All commands need to be executed like so: 
## "make auth-shell"
##

## Command run everything
up:
	cd authService && docker-compose up -d
	cd todoService && docker-compose up -d

## Command remove everything
down:
	cd authService && docker-compose down
	cd todoService && docker-compose down

## Command for open the docker container for auth service
auth-shell:
	docker exec -it todo-hexagonal-authService-1 bash

## Command for open the docker container for auth service
todo-shell:
	docker exec -it todo-hexagonal-todoService-1 bash

## Command for check the node version in the auth service
auth-node-v:
	docker exec -it todo-hexagonal-authService-1 bash -c "node -v"